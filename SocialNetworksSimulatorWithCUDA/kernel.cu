﻿#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <string>
#include <stdio.h>
#include <vector>
#include <list>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <ctime>

#define BLOCK_SIZE 16

using namespace std;

void cpu_matrix_mult(float *h_a, float *h_b, float *h_result, int m, int n, int k, int activityCount, float *h_t) {
	int currentActivityCount = -1;
	int during = 0;
	float *h_b_new, *h_b_calculate;
	cudaMallocHost((void **)&h_b_new, sizeof(int)*m*k);
	cudaMallocHost((void **)&h_b_calculate, sizeof(int)*m*k);

	memcpy(h_b_calculate, h_b, sizeof(int)*m*k);

	while (activityCount > currentActivityCount)
	{
		if (currentActivityCount > -1)
		{
			memcpy(h_b_calculate, h_b_new, sizeof(int)*m*k);
		}

		currentActivityCount = activityCount;
		activityCount = 0;

		for (int i = 0; i < m; ++i)
		{
			for (int j = 0; j < k; ++j)
			{
				float tmp = 0.0;
				for (int h = 0; h < n; ++h)
				{
					tmp += h_a[i * n + h] * h_b_calculate[h * k + j];
				}
				h_result[i * k + j] = tmp;
				if (tmp >= h_t[i])
				{
					activityCount++;
					h_b_new[i * k + j] = 1;
				}
				else
				{
					h_b_new[i * k + j] = 0;
				}
			}
		}

		during++;
		printf("Liczba aktywnych: %d Przebieg: %d\n", activityCount, during);
	}
}

__global__ void gpu_matrix_mult(float *a, float *b, float *c, int m, int n, int k)
{
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	float sum = 0;
	if (col < k && row < m)
	{
		for (int i = 0; i < n; i++)
		{
			sum += a[row * n + i] * b[i * k + col];
		}
		c[row * k + col] = sum;
	}
}

int main(int argc, char const *argv[])
{
	int m, n, k;

	string datasetPath; //ścieżka datasetu
	string activityMatrixPath; //ścieżka macierzy aktywacji
	string cpuResultPath;
	string gpuResultPath;
	float theta;

	//
	typedef vector<string> StringVector;
	list<StringVector> rows;
	list<StringVector>::iterator it;

	datasetPath = argv[1];//"E:\\PWr\\magisterka\\dataset\\datasetOut3.txt";
	activityMatrixPath = argv[2]; //"E:\\PWr\\magisterka\\dataset\\activityMatrix3.txt";
	if (argv[3] != NULL)
	{
		theta = atof(argv[3]);
	}
	else
	{
		theta = 0.5;
	}
	//cpuResultPath = argv[2]; //"E:\\PWr\\magisterka\\test\\cpuResult.txt";
	//gpuResultPath = argv[3]; //"E:\\PWr\\magisterka\\test\\gpuResult.txt";

	//datasetPath = "E:\\PWr\\magisterka\\dataset\\datasetOut4.txt";
	//activityMatrixPath = "E:\\PWr\\magisterka\\dataset\\activityMatrix4.txt";
	cpuResultPath = "E:\\PWr\\magisterka\\test\\cpuResult.txt";
	gpuResultPath = "E:\\PWr\\magisterka\\test\\gpuResult.txt";


	ifstream file(datasetPath);
	int size = 0;

	//wczytanie datasetu z pliku
	if (file.is_open())
	{
		while (file)
		{
			vector<string> stringVector;
			char line[255];
			file.getline(line, 255);
			string stringLine(line);

			istringstream iss(stringLine);
			int count = 0;

			do
			{
				string subs;
				iss >> subs;
				if (subs != "")
				{
					stringVector.push_back(subs);
					count++;

					//wyznaczamy ile jest wierzchołków
					if (atoi(stringVector[0].c_str()) > size)
						size = atoi(stringVector[0].c_str());
				}
			} while (iss);

			rows.push_back({ stringVector });
		}
	}

	m = size;
	n = size;
	k = 1;

	printf("cudastart\n");

	// alokacjja pamięci RAM
	float *h_a, *h_b, *h_c, *h_cc, *h_t;
	cudaMallocHost((void **)&h_a, sizeof(int)*m*n);
	cudaMallocHost((void **)&h_b, sizeof(int)*n*k);
	cudaMallocHost((void **)&h_c, sizeof(int)*m*k);
	cudaMallocHost((void **)&h_cc, sizeof(int)*m*k);
	cudaMallocHost((void **)&h_t, sizeof(int)*n*k);

	// inicjalizacja macierzy A
	for (int i = 0; i < m; ++i) {
		for (int j = 0; j < n; ++j) {
			h_a[i * n + j] = 0;
		}
	}

	// inicjalizacja macierzy B
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < k; ++j) {
			h_b[i * k + j] = 0;
		}
	}

	int rowsCount = rows.size();
	int iterator;

	//wyznaczenie połączeń pomiędzy węzłami (z sumowaniem)
	for (iterator = 0, it = rows.begin(); iterator < rowsCount - 1; iterator++)
	{
		vector<string> stringVector = *it;
		const int w = atoi(stringVector[0].c_str()) - 1;
		const int d = atoi(stringVector[1].c_str()) - 1;
		const float weight = atof(stringVector[2].c_str());

		if (stringVector.size() > 3)
		{
			const float thetaFile = atof(stringVector[3].c_str());
			h_t[iterator] = thetaFile;
		}
		else
		{
			h_t[iterator] = theta;
		}
		h_a[w * n + d] = weight;
		++it;
	}

	//wczytanie macierzy aktywności z pliku
	ifstream file2(activityMatrixPath);

	if (file2.is_open())
	{
		int count = 0;

		while (file2)
		{
			vector<string> stringVector;
			char line[255];
			file2.getline(line, 255);
			string stringLine(line);

			istringstream iss(stringLine);

			do
			{
				string subs;
				iss >> subs;
				if (subs != "")
				{
					h_b[count * k + 0] = atoi(subs.c_str());
					count++;
				}
			} while (iss);
		}
	}

	double gpu_elapsed_time_ms, cpu_elapsed_time_ms;

	/*-------------------------------------------------------------------- CPU VERSION ---------------------------------------------------------------------*/

	int activityCount = 0;

	printf("CPU PROCESS\n");
	clock_t begin = clock();

	cpu_matrix_mult(h_a, h_b, h_cc, m, n, k, activityCount, h_t);

	clock_t end = clock();

	cpu_elapsed_time_ms = double(end - begin) / CLOCKS_PER_SEC;

	printf("Time elapsed on matrix multiplication of %dx%d . %dx%d on CPU: %f ms.\n\n", m, n, n, k, cpu_elapsed_time_ms);

	/*-------------------------------------------------------------------- GPU VERSION -------------------------------------------------------------------*/

	// rozpoczęcie mierzenia czasu obliczeń GPU
	printf("GPU PROCESS\n");
	begin = clock();
	// Alikacja pamięci na GPU
	float *d_a, *d_b, *d_c;
	cudaMalloc((void **)&d_a, sizeof(int)*m*n);
	cudaMalloc((void **)&d_b, sizeof(int)*n*k);
	cudaMalloc((void **)&d_c, sizeof(int)*m*k);

	// skopiowanie macierzy A i B z pamięci RAM do GPU
	cudaMemcpy(d_a, h_a, sizeof(int)*m*n, cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, h_b, sizeof(int)*n*k, cudaMemcpyHostToDevice);

	unsigned int grid_rows = (m + BLOCK_SIZE - 1) / BLOCK_SIZE;
	unsigned int grid_cols = (k + BLOCK_SIZE - 1) / BLOCK_SIZE;
	dim3 dimGrid(grid_cols, grid_rows);
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);

	activityCount = 0;

	float *h_b_new;
	cudaMallocHost((void **)&h_b_new, sizeof(int)*m*k);
	int during = 0;

	int currentActivityCount = -1;

	while (activityCount > currentActivityCount)
	{
		if (currentActivityCount > -1)
		{
			cudaMemcpy(d_b, h_b_new, sizeof(int)*m*k, cudaMemcpyHostToDevice);
		}

		gpu_matrix_mult << <dimGrid, dimBlock >> >(d_a, d_b, d_c, m, n, k);

		// Transefr results from device to host 
		cudaMemcpy(h_c, d_c, sizeof(int)*m*k, cudaMemcpyDeviceToHost);

		currentActivityCount = activityCount;
		activityCount = 0;

		for (int i = 0; i < m; ++i)
		{
			for (int j = 0; j < k; ++j)
			{
				if (h_c[i*k + j] >= h_t[i])
				{
					activityCount++;

					h_b_new[i * k + j] = 1;
				}
				else
				{
					h_b_new[i * k + j] = 0;
				}
			}
		}

		during++;
		printf("Liczba aktywnych: %d Przebieg: %d\n", activityCount, during);
	}


	// Transfer wyników z GPU do pamięci RAM
	cudaMemcpy(h_c, d_c, sizeof(int)*m*k, cudaMemcpyDeviceToHost);

	cudaThreadSynchronize();
	end = clock();

	printf("Liczba aktywnych: %d Przebieg: %d\n", activityCount, during);

	// wyliczenie czasu wykonywania obliczeń przez GPU
	gpu_elapsed_time_ms = double(end - begin) / CLOCKS_PER_SEC;
	printf("Time elapsed on matrix multiplication of %dx%d . %dx%d on GPU: %f ms.\n\n", m, n, n, k, gpu_elapsed_time_ms);

	// wyczyszczenie pamięci
	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_c);
	cudaFreeHost(h_a);
	cudaFreeHost(h_b);
	cudaFreeHost(h_c);
	cudaFreeHost(h_cc);

	return 0;
}
